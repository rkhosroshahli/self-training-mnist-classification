import numpy as np
import matplotlib.pyplot as plt
# Used for Predicting Model
from sklearn.linear_model import LogisticRegression
# Used for Calculating Result and Accuracy
from sklearn.metrics import f1_score
from sklearn.metrics import plot_confusion_matrix
# Used for getting Scaled Data
from load_mnist import get_data_from_train, load_data, get_data_from_train_and_test


train_idx = round(70000*0.1)
unlabeled_idx = train_idx + round(70000*0.86)
test_idx = round(10000*0.98)


def normal_train_mnist():
    X_train, y_train, X_test, y_test = load_data()
    logisticRegr = LogisticRegression(solver='lbfgs', max_iter=500)

    print("Training images without unlabeled datas...")

    logisticRegr.fit(X_train, y_train)

    y_test_pred = logisticRegr.predict(X_test)
    y_train_pred = logisticRegr.predict(X_train)

    train_f1 = f1_score(y_train, y_train_pred, average=None)
    test_f1 = f1_score(y_test, y_test_pred, average=None)

    print(f"Train f1 Score: {train_f1}")
    print(f"Test f1 Score: {test_f1}")

    # View confusion matrix after normal training
    _, ax = plt.subplots(figsize=(10, 10))
    plot_confusion_matrix(logisticRegr, X_test, y_test, cmap='Blues', normalize='true', display_labels=[
                          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'], ax=ax)
    plt.show()


def self_train_mnist():
    X_train, y_train, X_test, y_test, X_unlabeled = get_data_from_train(
        train_idx, unlabeled_idx, test_idx)

    # Change this function if you want to use both train and test data
    # X_train, y_train, X_test, y_test, X_unlabeled = get_data_from_train_and_test()

    logisticRegr = LogisticRegression(solver='lbfgs', max_iter=500)

    print("Training images with unlabeled datas...")

    train_f1s = []
    test_f1s = []
    pseudo_labels = []
    high_prob_counter = 1
    iteration = 0

    while high_prob_counter != 0:
        high_prob_counter = 0

        logisticRegr = LogisticRegression(solver='lbfgs', max_iter=500)

        logisticRegr.fit(X_train, y_train)

        y_test_pred = logisticRegr.predict(X_test)
        y_train_pred = logisticRegr.predict(X_train)

        train_f1 = f1_score(y_train, y_train_pred, average=None)
        test_f1 = f1_score(y_test, y_test_pred, average=None)

        print(f"Iteration: {iteration}")
        print(f"Train f1 Score: {train_f1}")
        print(f"Test f1 Score: {test_f1}")

        train_f1s.append(train_f1)
        test_f1s.append(test_f1)

        unlabeled_pred_prob = logisticRegr.predict_proba(X_unlabeled)

        print(f"Now predicting labels for unlabeled data...")

        high_prob_idx = []

        for i in range(unlabeled_pred_prob.shape[0]):
            max = 0
            max_j = 0
            pred_prob = unlabeled_pred_prob[i]

            for j in range(10):
                if pred_prob[j] > max:
                    max = pred_prob[j]
                    max_j = j

            if max > 0.99:
                high_prob_counter += 1
                high_prob_idx.append(i)
                X_train = np.append(X_train, [X_unlabeled[i]], axis=0)
                y_train = np.append(y_train, max_j)

        X_unlabeled = np.delete(X_unlabeled, high_prob_idx, axis=0)

        print(
            f"{high_prob_counter} high-probability predictions added to training data.")

        pseudo_labels.append(high_prob_counter)

        print(f"{len(X_unlabeled)} unlabeled instances remaining.")

        iteration += 1

    _, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(25, 25))
    ax1.plot(range(iteration), test_f1s)
    ax1.set_ylabel('f1 Score')
    ax2.bar(x=range(iteration), height=pseudo_labels)
    ax2.set_ylabel('Pseudo-Labels Created')
    ax2.set_xlabel('# Iterations')

    # View confusion matrix after self-training
    _, ax = plt.subplots(figsize=(10, 10))
    plot_confusion_matrix(logisticRegr, X_test, y_test, cmap='Blues', normalize='true',
                          display_labels=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'], ax=ax)


# Here we call our two function above

# First fit and predict train and test datas without any unlabeled data
normal_train_mnist()

# Second fit and predict train and test and then improve train datas with unlabeled data
self_train_mnist()
